from setuptools import setup

import os

# Put here required packages
packages = ['Django==1.7', 'Pillow==2.6.1', 'argparse==1.2.1', 'wsgiref==0.1.2']

if 'REDISCLOUD_URL' in os.environ and 'REDISCLOUD_PORT' in os.environ and 'REDISCLOUD_PASSWORD' in os.environ:
     packages.append('django-redis-cache')
     packages.append('hiredis')

setup(name='lab',
      version='0.4',
      description='New Lab for InternetDevels',
      author='Voron',
      author_email='m.keda@internetdevels.ua',
      url='http://internetdevels.ua',
      install_requires=packages,
)

