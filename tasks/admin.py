from django.contrib import admin
from tasks.models import Project, Worklog, Task, Comment

admin.site.register(Project)
admin.site.register(Worklog)
admin.site.register(Task)
admin.site.register(Comment)