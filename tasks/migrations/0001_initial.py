# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Comment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('body', models.TextField()),
                ('created_date', models.DateTimeField(default=datetime.date(2014, 11, 3), auto_now_add=True)),
                ('changed_date', models.DateTimeField(default=datetime.date(2014, 11, 3), auto_now=True)),
                ('author', models.ForeignKey(related_name=b'comment author', default=b'', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ['-created_date'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Project',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=60)),
                ('note', models.TextField(null=True, blank=True)),
                ('completed', models.BooleanField(default=False)),
                ('created_date', models.DateTimeField(default=datetime.date(2014, 11, 3), auto_now_add=True)),
                ('changed_date', models.DateTimeField(default=datetime.date(2014, 11, 3), auto_now=True)),
                ('slug', models.SlugField(max_length=60, editable=False)),
                ('created_by', models.ForeignKey(related_name=b'project_created_by', default=b'', to=settings.AUTH_USER_MODEL)),
                ('users', models.ManyToManyField(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ['completed', 'created_date'],
                'verbose_name_plural': 'Projects',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Task',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=140)),
                ('note', models.TextField(null=True, blank=True)),
                ('priority', models.PositiveIntegerField(default=2, max_length=4)),
                ('created_date', models.DateTimeField(default=datetime.date(2014, 11, 3), auto_now_add=True)),
                ('changed_date', models.DateTimeField(default=datetime.date(2014, 11, 3), auto_now=True)),
                ('status', models.PositiveIntegerField(default=1, max_length=4)),
                ('slug', models.SlugField(max_length=60, editable=False)),
                ('assigned_to', models.ForeignKey(related_name=b'assigned_to', to=settings.AUTH_USER_MODEL)),
                ('created_by', models.ForeignKey(related_name=b'task_created_by', default=b'', to=settings.AUTH_USER_MODEL)),
                ('project', models.ForeignKey(to='tasks.Project')),
            ],
            options={
                'ordering': ['priority', 'project', '-created_date'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Worklog',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('note', models.CharField(max_length=500)),
                ('time_spend', models.TimeField()),
                ('created_date', models.DateTimeField(default=datetime.date(2014, 11, 3), auto_now_add=True)),
                ('changed_date', models.DateTimeField(default=datetime.date(2014, 11, 3), auto_now=True)),
                ('created_by', models.ForeignKey(related_name=b'worklog author', default=b'', to=settings.AUTH_USER_MODEL)),
                ('task', models.ForeignKey(to='tasks.Task')),
            ],
            options={
                'ordering': ['-created_date'],
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='comment',
            name='task',
            field=models.ForeignKey(to='tasks.Task'),
            preserve_default=True,
        ),
    ]
