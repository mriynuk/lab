# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import datetime


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('tasks', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='project',
            name='menegers',
            field=models.ManyToManyField(related_name=b'menegers', to=settings.AUTH_USER_MODEL),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='comment',
            name='changed_date',
            field=models.DateTimeField(default=datetime.date(2014, 11, 10), auto_now=True),
        ),
        migrations.AlterField(
            model_name='comment',
            name='created_date',
            field=models.DateTimeField(default=datetime.date(2014, 11, 10), auto_now_add=True),
        ),
        migrations.AlterField(
            model_name='project',
            name='changed_date',
            field=models.DateTimeField(default=datetime.date(2014, 11, 10), auto_now=True),
        ),
        migrations.AlterField(
            model_name='project',
            name='created_date',
            field=models.DateTimeField(default=datetime.date(2014, 11, 10), auto_now_add=True),
        ),
        migrations.AlterField(
            model_name='project',
            name='users',
            field=models.ManyToManyField(related_name=b'users', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='task',
            name='assigned_to',
            field=models.ForeignKey(related_name=b'assigned_to', verbose_name='\u043d\u0430\u043f\u0440\u0430\u0432\u043b\u0435\u043d\u043e \u043d\u0430', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='task',
            name='changed_date',
            field=models.DateTimeField(default=datetime.date(2014, 11, 10), verbose_name='changed date', auto_now=True),
        ),
        migrations.AlterField(
            model_name='task',
            name='created_by',
            field=models.ForeignKey(related_name=b'task_created_by', default=b'', verbose_name='created by', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='task',
            name='created_date',
            field=models.DateTimeField(default=datetime.date(2014, 11, 10), verbose_name='created date', auto_now_add=True),
        ),
        migrations.AlterField(
            model_name='task',
            name='note',
            field=models.TextField(null=True, verbose_name='\u043e\u043f\u0438\u0441', blank=True),
        ),
        migrations.AlterField(
            model_name='task',
            name='priority',
            field=models.PositiveIntegerField(default=2, max_length=4, verbose_name='\u043f\u0440\u0456\u043e\u0440\u0438\u0442\u0435\u0442'),
        ),
        migrations.AlterField(
            model_name='task',
            name='status',
            field=models.PositiveIntegerField(default=1, max_length=4, verbose_name='\u0441\u0442\u0430\u0442\u0443\u0441'),
        ),
        migrations.AlterField(
            model_name='task',
            name='title',
            field=models.CharField(max_length=140, verbose_name='\u0437\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a'),
        ),
        migrations.AlterField(
            model_name='worklog',
            name='changed_date',
            field=models.DateTimeField(default=datetime.date(2014, 11, 10), auto_now=True),
        ),
        migrations.AlterField(
            model_name='worklog',
            name='created_date',
            field=models.DateTimeField(default=datetime.date(2014, 11, 10), auto_now_add=True),
        ),
    ]
