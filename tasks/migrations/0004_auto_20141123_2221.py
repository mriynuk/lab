# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('tasks', '0003_auto_20141111_2323'),
    ]

    operations = [
        migrations.AlterField(
            model_name='comment',
            name='changed_date',
            field=models.DateTimeField(default=datetime.date(2014, 11, 23), auto_now=True),
        ),
        migrations.AlterField(
            model_name='comment',
            name='created_date',
            field=models.DateTimeField(default=datetime.date(2014, 11, 23), auto_now_add=True),
        ),
        migrations.AlterField(
            model_name='project',
            name='changed_date',
            field=models.DateTimeField(default=datetime.date(2014, 11, 23), auto_now=True),
        ),
        migrations.AlterField(
            model_name='project',
            name='created_date',
            field=models.DateTimeField(default=datetime.date(2014, 11, 23), auto_now_add=True),
        ),
        migrations.AlterField(
            model_name='task',
            name='changed_date',
            field=models.DateTimeField(default=datetime.date(2014, 11, 23), verbose_name='changed date', auto_now=True),
        ),
        migrations.AlterField(
            model_name='task',
            name='created_date',
            field=models.DateTimeField(default=datetime.date(2014, 11, 23), verbose_name='created date', auto_now_add=True),
        ),
        migrations.AlterField(
            model_name='task',
            name='priority',
            field=models.CharField(default=2, max_length=4, verbose_name='\u043f\u0440\u0456\u043e\u0440\u0438\u0442\u0435\u0442', choices=[(b'1', 'Blocker'), (b'2', 'Critical'), (b'3', 'Major'), (b'4', 'Minor')]),
        ),
        migrations.AlterField(
            model_name='worklog',
            name='changed_date',
            field=models.DateTimeField(default=datetime.date(2014, 11, 23), auto_now=True),
        ),
        migrations.AlterField(
            model_name='worklog',
            name='created_date',
            field=models.DateTimeField(default=datetime.date(2014, 11, 23), auto_now_add=True),
        ),
    ]
