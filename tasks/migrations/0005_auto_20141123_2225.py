# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tasks', '0004_auto_20141123_2221'),
    ]

    operations = [
        migrations.AlterField(
            model_name='task',
            name='status',
            field=models.CharField(default=1, max_length=4, verbose_name='\u0441\u0442\u0430\u0442\u0443\u0441', choices=[(b'1', '\u0412\u0456\u0434\u043a\u0440\u0438\u0442\u043e'), (b'2', '\u0412 \u043e\u0447\u0456\u043a\u0443\u0432\u0430\u043d\u043d\u0456'), (b'3', '\u0412\u0438\u043a\u043e\u043d\u0430\u043d\u043e'), (b'4', '\u0417\u0430\u043a\u0440\u0438\u0442\u043e')]),
        ),
    ]
