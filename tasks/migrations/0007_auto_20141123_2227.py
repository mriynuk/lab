# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tasks', '0006_auto_20141123_2226'),
    ]

    operations = [
        migrations.AlterField(
            model_name='task',
            name='priority',
            field=models.PositiveIntegerField(default=2, max_length=4, verbose_name='\u043f\u0440\u0456\u043e\u0440\u0438\u0442\u0435\u0442', choices=[(b'1', 'Blocker'), (b'2', 'Critical'), (b'3', 'Major'), (b'4', 'Minor')]),
        ),
    ]
