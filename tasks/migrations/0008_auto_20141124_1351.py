# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('tasks', '0007_auto_20141123_2227'),
    ]

    operations = [
        migrations.AlterField(
            model_name='comment',
            name='changed_date',
            field=models.DateTimeField(default=datetime.date(2014, 11, 24), auto_now=True),
        ),
        migrations.AlterField(
            model_name='comment',
            name='created_date',
            field=models.DateTimeField(default=datetime.date(2014, 11, 24), auto_now_add=True),
        ),
        migrations.AlterField(
            model_name='project',
            name='changed_date',
            field=models.DateTimeField(default=datetime.date(2014, 11, 24), auto_now=True),
        ),
        migrations.AlterField(
            model_name='project',
            name='created_date',
            field=models.DateTimeField(default=datetime.date(2014, 11, 24), auto_now_add=True),
        ),
        migrations.AlterField(
            model_name='task',
            name='changed_date',
            field=models.DateTimeField(default=datetime.date(2014, 11, 24), verbose_name='changed date', auto_now=True),
        ),
        migrations.AlterField(
            model_name='task',
            name='created_date',
            field=models.DateTimeField(default=datetime.date(2014, 11, 24), verbose_name='created date', auto_now_add=True),
        ),
        migrations.AlterField(
            model_name='worklog',
            name='changed_date',
            field=models.DateTimeField(default=datetime.date(2014, 11, 24), auto_now=True),
        ),
        migrations.AlterField(
            model_name='worklog',
            name='created_date',
            field=models.DateTimeField(default=datetime.date(2014, 11, 24), auto_now_add=True),
        ),
    ]
