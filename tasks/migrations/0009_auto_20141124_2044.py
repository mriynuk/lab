# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tasks', '0008_auto_20141124_1351'),
    ]

    operations = [
        migrations.AlterField(
            model_name='task',
            name='priority',
            field=models.PositiveIntegerField(default=2, max_length=4, verbose_name='\u043f\u0440\u0456\u043e\u0440\u0438\u0442\u0435\u0442', choices=[(1, 'Blocker'), (2, 'Critical'), (3, 'Major'), (4, 'Minor')]),
        ),
        migrations.AlterField(
            model_name='task',
            name='status',
            field=models.PositiveIntegerField(default=1, max_length=4, verbose_name='\u0441\u0442\u0430\u0442\u0443\u0441', choices=[(1, '\u0412\u0456\u0434\u043a\u0440\u0438\u0442\u043e'), (2, '\u0412 \u043e\u0447\u0456\u043a\u0443\u0432\u0430\u043d\u043d\u0456'), (3, '\u0412\u0438\u043a\u043e\u043d\u0430\u043d\u043e'), (4, '\u0417\u0430\u043a\u0440\u0438\u0442\u043e')]),
        ),
    ]
