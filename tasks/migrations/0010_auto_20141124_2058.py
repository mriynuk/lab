# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tasks', '0009_auto_20141124_2044'),
    ]

    operations = [
        migrations.AlterField(
            model_name='task',
            name='priority',
            field=models.PositiveIntegerField(default=3, max_length=5, verbose_name='\u043f\u0440\u0456\u043e\u0440\u0438\u0442\u0435\u0442', choices=[(1, 'Blocker'), (2, 'Critical'), (3, 'Major'), (4, 'Minor'), (5, 'Trivial')]),
        ),
    ]
