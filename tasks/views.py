from django.core.paginator import Paginator, InvalidPage, EmptyPage, PageNotAnInteger
from django.core.exceptions import PermissionDenied
from django.core.context_processors import csrf
from django.shortcuts import redirect, get_object_or_404, render_to_response
from django.contrib.auth.models import User
from tasks.models import Project, Worklog, WorklogForm, Task, TaskCreateForm, TaskEditForm, Comment, CommentForm
from django.db.models import Q
import sys
from django.contrib.auth.decorators import login_required
from django.utils.translation import ugettext as _

base_submenu = [
        [_('Main page'), '/'],
        [_('My worklogs'), '/worklogs'],
        [_('Projects'), '/projects'],
        [_('Map'), '/map'],
        [_('Logout'), '/logout'],
    ]

def paginate(objects, page, num):
    """Function for paginate."""
    paginator = Paginator(objects, num)
    try:
        objects = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        objects = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        objects = paginator.page(paginator.num_pages)
    return objects

@login_required(login_url='/login')
def main(request):
    """Main page. List of tasks."""
    tasks = Task.objects.filter(Q(assigned_to=request.user) | Q(created_by=request.user)).order_by("-priority", "project", "created_date")
    page = request.GET.get('page')
    tasks = paginate(tasks, page, 10)

    return render_to_response("list.html", dict(tasks=tasks, user=request.user, link=(_('Projects'), '/projects'), submenu=base_submenu))

@login_required(login_url='/login')
def worklogs(request, id=False):
    """Show your worklogs or worklogs of task."""
    if id:
        worklogs = Worklog.objects.filter(Q(task=id) & ~Q(created_by=request.user)).order_by("-created_date")
        worklogs_my = Worklog.objects.filter(Q(created_by=request.user) & Q(task=id)).order_by("-created_date")
    else:
        worklogs = Worklog.objects.filter(created_by=request.user).order_by("-created_date")
        worklogs_my = ()

    page = request.GET.get('page')
    worklogs = paginate(worklogs, page, 10)

    return render_to_response("worklogs.html", dict(worklogs=worklogs, worklogs_my=worklogs_my, user=request.user, submenu=base_submenu))

@login_required(login_url='/login')
def projects(request):
    """Show your projects."""
    projects = Project.objects.filter(users=request.user).order_by("completed", "created_date")

    page = request.GET.get('page')
    projects = paginate(projects, page, 10)

    return render_to_response("projects.html", dict(projects=projects, user=request.user, submenu=base_submenu))

@login_required(login_url='/login')
def showproject(request, pk):
    """Single project and related tasks."""
    project = get_object_or_404(Project, pk=int(pk))
    tasks = Task.objects.filter(project_id=int(pk)).order_by("status", "-priority", "created_date")
    page = request.GET.get('page')
    tasks = paginate(tasks, page, 10)
    submenu = [[_('Create task'), '/createtask?project='+str(project.id)]]

    c = dict(project=project, tasks=tasks, user=request.user, link=(_('New task'), '/project/'+str(project.id)+'/newtask'), submenu=base_submenu+submenu)
    c.update(csrf(request))

    return render_to_response("project.html", c)

@login_required(login_url='/login')
def showprojecttasks(request, pk):
    """All tasks related to user and project."""
    tasks = Task.objects.filter((Q(assigned_to=request.user) | Q(created_by=request.user)) & Q(project_id=pk)).order_by("-priority", "project", "created_date")
    page = request.GET.get('page')
    tasks = paginate(tasks, page, 10)

    return render_to_response("list.html", dict(tasks=tasks, user=request.user, submenu=base_submenu))

@login_required(login_url='/login')
def showtask(request, pk):
    """Single task with comments and a comment form."""
    submenu = []
    task = get_object_or_404(Task, pk=int(pk))
    comments = Comment.objects.filter(task_id=task.id).order_by("-created_date")

    page = request.GET.get('page')
    comments = paginate(comments, page, 3)

    form = CommentForm(request.POST or None)

    if form.is_valid():
        submission = form.save(commit=False)
        submission.author = request.user
        submission.task = task
        submission.save()
    else:
        form = form

    if task.assigned_to == request.user:
        link=(_('Add worklog'), '/task/' + str(task.id) + '/addworklog')
        submenu = [[_('View worklog of') + ' ' + task.title, '/task/'+str(task.id)+'/worklogs']]
    elif task.created_by == request.user or request.user.is_staff:
        link=(_('View worklog'), '/task/' + str(task.id) + '/worklogs')
    else:
        form=None

    c = dict(task=task, comments=comments, user=request.user, form=form, link=link, submenu=base_submenu+submenu)
    c.update(csrf(request))

    return render_to_response("task.html", c)

@login_required(login_url='/login')
def newtask(request, id=False):
    """Form to create new task."""

    if id:
        project = get_object_or_404(Project, pk=int(id))
        if request.user not in project.menegers.all() and not request.user.is_staff:
            raise PermissionDenied
        form = TaskCreateForm(request.POST or None, initial={'project': id})
        form.fields['assigned_to'].queryset = project.users.all() | project.menegers.all()

    else:
        raise PermissionDenied


    if request.POST and form.is_valid():
        submission = form.save(commit=False)
        submission.created_by = request.user
        submission.status = 1
        submission.project_id = int(id)
        submission.save()
        return redirect('/project/'+id)

    c = dict(user=request.user, form=form, create_mode=True, submenu=base_submenu)
    c.update(csrf(request))

    return render_to_response("createtask.html", c)

@login_required(login_url='/login')
def edittask(request, id=False):
    """Form to create new task."""

    if id:
        task = get_object_or_404(Task, id=int(id))
        project = get_object_or_404(Project, pk=task.project_id)
        form = TaskEditForm(request.POST or None, instance=task)
        form.fields['assigned_to'].queryset = project.users.all() | project.menegers.all()

        create_mode = False
    else:
        raise PermissionDenied

    if request.POST:
        if id:
            form = TaskEditForm(request.POST, instance=task)
        else:
            raise PermissionDenied
            
        if form.is_valid():
            submission = form.save(commit=False)
            submission.created_by = request.user
            if not id:
                submission.status = 1
            else:
                submission.project = task.project
            submission.save()
            return redirect('/')

    c = dict(user=request.user, form=form, create_mode=False, submenu=base_submenu)
    c.update(csrf(request))

    return render_to_response("createtask.html", c)

# @login_required(login_url='/login')
# def posting(request):
#     """Create new task."""
#     form = TaskForm(request.POST or None)
#     if request.method == 'POST':
#         form = TaskForm(request.user, request.POST)
#     else:
#         form = TaskForm(request.POST)
#     if form.is_valid():
#         submission = form.save(commit=False)
#         submission.created_by_id = request.user.id
#         submission.save()
#     else:
#         form = form

#     return redirect('/')

@login_required(login_url='/login')
def addworklog(request, id=False):
    """Form to add worklog."""
    if id:
        task = get_object_or_404(Task, id=int(id))
        if request.user != task.assigned_to:
            raise PermissionDenied
        form = WorklogForm(request.POST or None)

    c = dict(user=request.user, form=form, submiturl='/task/' + str(task.id) + '/postworklog', submenu=base_submenu)
    c.update(csrf(request))

    return render_to_response("addworklog.html", c)

@login_required(login_url='/login')
def postworklog(request, id=False):
    """Add worklog."""
    form = WorklogForm()
    if id:
        task = get_object_or_404(Task, id=int(id))
        if request.user != task.assigned_to:
            raise PermissionDenied

        if request.method == 'POST':
            form = WorklogForm(request.POST or None)

        if form.is_valid():
            submission = form.save(commit=False)
            submission.created_by = request.user
            submission.task = task
            submission.save()
        else:
            form = form

    return redirect('/')

@login_required(login_url='/login')
def deletetask(request, id):
    """Delete task."""
    task = get_object_or_404(Task, id=int(id))
    if request.user.id != task.created_by_id and not request.user.is_staff:
        raise PermissionDenied
    task.delete()
    return redirect('/')
