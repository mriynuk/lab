from django.contrib import admin
from users.models import Statistic, Profile

admin.site.register(Statistic)
admin.site.register(Profile)
