# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Profile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('middle_name', models.CharField(default=b'exemple', max_length=40)),
                ('picture', models.ImageField(default=b'/static/no-avatar.jpg', upload_to=b'avatars')),
                ('birthday', models.DateField(default=datetime.date.today)),
                ('relationship_status', models.CharField(default=1, max_length=10, choices=[(b'1', 'Single'), (b'2', 'In a relationship'), (b'3', 'Engaged'), (b'4', 'Married'), (b'5', "It's complicated"), (b'6', 'In an open relationship'), (b'7', 'Widowed'), (b'8', 'Separated'), (b'9', 'Divorced')])),
                ('skype', models.CharField(default=b'exemple', max_length=40)),
                ('corporative_skype', models.CharField(default=b'id.exemple', max_length=40)),
                ('corporative_email', models.EmailField(default=b'exemple@internetdevels.ua', max_length=40)),
                ('phone_number', models.CharField(default=b'+380123456789', max_length=15)),
                ('facebook_profile', models.URLField(default=b'https://facebook.com/exemple', max_length=60)),
                ('map_x_pos', models.PositiveIntegerField(default=0, max_length=1625)),
                ('map_y_pos', models.PositiveIntegerField(default=0, max_length=1950)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL, unique=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Statistic',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_date', models.DateTimeField(default=datetime.date(2014, 11, 3), auto_now_add=True)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ['-created_date'],
            },
            bases=(models.Model,),
        ),
    ]
