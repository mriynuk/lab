# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='birthday',
            field=models.DateField(default=datetime.date.today, verbose_name='\u0434\u0435\u043d\u044c \u043d\u0430\u0440\u043e\u0434\u0436\u0435\u043d\u043d\u044f'),
        ),
        migrations.AlterField(
            model_name='profile',
            name='corporative_email',
            field=models.EmailField(default=b'exemple@internetdevels.ua', max_length=40, verbose_name='\u043a\u043e\u0440\u043f\u043e\u0440\u0430\u0442\u0438\u0432\u043d\u0430 \u043f\u043e\u0448\u0442\u0430'),
        ),
        migrations.AlterField(
            model_name='profile',
            name='corporative_skype',
            field=models.CharField(default=b'id.exemple', max_length=40, verbose_name='\u043a\u043e\u0440\u043f\u043e\u0440\u0430\u0442\u0438\u0432\u043d\u0438\u0439 skype'),
        ),
        migrations.AlterField(
            model_name='profile',
            name='facebook_profile',
            field=models.URLField(default=b'https://facebook.com/exemple', max_length=60, verbose_name='\u043f\u0440\u043e\u0444\u0456\u043b\u044c \u0443 Facebook'),
        ),
        migrations.AlterField(
            model_name='profile',
            name='middle_name',
            field=models.CharField(default=b'exemple', max_length=40, verbose_name='Middle name'),
        ),
        migrations.AlterField(
            model_name='profile',
            name='phone_number',
            field=models.CharField(default=b'+380123456789', max_length=15, verbose_name='\u043d\u043e\u043c\u0435\u0440 \u0442\u0435\u043b\u0435\u0444\u043e\u043d\u0443'),
        ),
        migrations.AlterField(
            model_name='profile',
            name='picture',
            field=models.ImageField(default=b'/static/no-avatar.jpg', upload_to=b'avatars', verbose_name='picture'),
        ),
        migrations.AlterField(
            model_name='profile',
            name='relationship_status',
            field=models.CharField(default=1, max_length=10, verbose_name='\u0441\u0456\u043c\u0435\u0439\u043d\u0438\u0439 \u0441\u0442\u0430\u0442\u0443\u0441', choices=[(b'1', '\u0411\u0435\u0437 \u043f\u0430\u0440\u0438'), (b'2', '\u0423 \u0441\u0442\u043e\u0441\u0443\u043d\u043a\u0430\u0445'), (b'3', '\u0417\u0430\u0440\u0443\u0447\u0435\u043d\u0438\u0439(\u0437\u0430\u0440\u0443\u0447\u0435\u043d\u0430)'), (b'4', '\u041e\u0434\u0440\u0443\u0436\u0435\u043d\u0438\u0439'), (b'5', '\u0412\u0441\u0435 \u0441\u043a\u043b\u0430\u0434\u043d\u043e'), (b'6', '\u0423 \u0432\u0456\u0434\u043a\u0440\u0438\u0442\u0438\u0445 \u0441\u0442\u043e\u0441\u0443\u043d\u043a\u0430\u0445'), (b'7', '\u041e\u0432\u0434\u043e\u0432\u0456\u043b\u0438\u0439(\u043e\u0432\u0434\u043e\u0432\u0456\u043b\u0430)'), (b'8', '\u0420\u043e\u0437\u043b\u0443\u0447\u0435\u043d\u0438\u0439(\u0440\u043e\u0437\u043b\u0443\u0447\u0435\u043d\u0430)'), (b'9', '\u0420\u043e\u0437\u043b\u0443\u0447\u0435\u043d\u0438\u0439(\u0440\u043e\u0437\u043b\u0443\u0447\u0435\u043d\u0430)')]),
        ),
        migrations.AlterField(
            model_name='profile',
            name='skype',
            field=models.CharField(default=b'exemple', max_length=40, verbose_name='skype'),
        ),
        migrations.AlterField(
            model_name='statistic',
            name='created_date',
            field=models.DateTimeField(default=datetime.date(2014, 11, 23), auto_now_add=True),
        ),
    ]
