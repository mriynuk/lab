from django.shortcuts import redirect, render_to_response, get_object_or_404
from django.core.context_processors import csrf
from django.contrib.auth.models import auth, User
from users.models import Statistic, Profile, ProfileForm, UserForm
from django.db.models import Q
import datetime
from django.contrib.auth.decorators import login_required
from django.utils.translation import ugettext as _
import sys
from django.utils import timezone

base_submenu = [
        [_('Main page'), '/'],
        [_('Create task'), '/createtask'],
        [_('My worklogs'), '/worklogs'],
        [_('Projects'), '/projects'],
        [_('Map'), '/map'],
        [_('Logout'), '/logout'],
    ]

def login(request):
    """Login form."""

    if request.method == 'POST':
        username = request.POST.get('username', '')
        password = request.POST.get('password', '')
        user = auth.authenticate(username=username, password=password)

        if user is not None and user.is_active:
            auth.login(request, user)
            log = Statistic(user=user)
            prev_log = Statistic.objects.filter(Q(user=user) & Q(created_date__gt=datetime.date.today()))
            if not prev_log:
                log.save()
                request.session.set_expiry(50400)
            try:
                resirect_url = request.META.get('HTTP_REFERER').split('?next=')[1]
            except IndexError:
                resirect_url = '/'
            return redirect(resirect_url)
        else:
            c = dict(username=username, error_message=_('Wrong password or User name'))
            c.update(csrf(request))
            return render_to_response("loginform.html", c)
    else:
        c = {}
        c.update(csrf(request))

        return render_to_response("loginform.html", c)

def logout(request):
    """Logout."""

    auth.logout(request)
    return redirect('/login')

@login_required(login_url='/login')
def showuser(request, pk):
    """User profile."""

    user = get_object_or_404(User, pk=int(pk))
    profile, created = Profile.objects.get_or_create(user=user)
    user_form = UserForm(request.POST or None, instance=user)
    profile_form = ProfileForm(request.POST or None, instance=profile)

    if user.id == request.user.id or request.user.is_staff:
        editable = True
        if user_form.is_valid():
            submission = user_form.save(commit=False)
            submission.save()
        else:
            user_form = user_form

        if profile_form.is_valid():
            submission = profile_form.save(commit=False)
            submission.save()
        else:
            profile_form = profile_form
    else:
        editable = False

    submenu = [[_('User statistic'), pk+'/statistic']]

    c = dict(user=request.user, profile_user=user, profile=profile, user_form=user_form, profile_form=profile_form, editable=editable, submenu=base_submenu+submenu)
    c.update(csrf(request))
    return render_to_response("userprofile.html", c)

@login_required(login_url='/login')
def map(request):
    """Users map."""

    users = Profile.objects.all().select_related('user')
    return render_to_response("map.html", dict(user=request.user, users=users, submenu=base_submenu))

@login_required(login_url='/login')
def userstatistic(request, pk, days=30):
    """Users statistic."""

    user = get_object_or_404(User, pk=int(pk))
    statistic = list(Statistic.objects.filter(Q(user=user) & Q(created_date__gt=(datetime.date.today() - datetime.timedelta(days=days)))))

    statistic_days = [stat.created_date.date() for stat in statistic]

    calendar = []
    user_delays = []

    for i in reversed(range(days)):
        day = datetime.date.today() - datetime.timedelta(days=i)
        calendar.append(day.strftime('%m/%d'))
        if day in statistic_days:
            user_delays.append(timezone.localtime(statistic.pop().created_date).strftime('%H:%M'))
        else:
            user_delays.append('---')

    return render_to_response("userstatistic.html", dict(user=request.user, statistic_user=user, calendar=calendar, user_delays=user_delays, submenu=base_submenu))
